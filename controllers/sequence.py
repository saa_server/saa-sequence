from bottle import get, request, run

import json

from datetime import datetime


_SECRET = "8ce91b66156d085bfe223d5dbd80949c"

@get('/sequence/getNext/<sequenceName>')
#@validateSession()
def sequence(rdb, sequenceName):
    """Get next unique id into sequence"""

    secret = request.query.get('secret')
    if secret != _SECRET:
        print('Invalid secret key')
        return {}

    userName = request.query.get('userName')

    nextId = _getNextId(rdb, sequenceName)

    _updateSequence(rdb, sequenceName, nextId, userName)

    return {'id': nextId}

def _getNextId(rdb, sequenceName):
    """Actually get next unique id from table"""
    nextId = rdb.hget('sequence', sequenceName)
    
    return int(nextId) if nextId is not None else 0


def _updateSequence(rdb, sequenceName, nextId, userName):
    """Increment table item with next id"""
    
    rdb.hincrby('sequence', sequenceName, 1)

    _saveHistory(rdb, sequenceName, nextId, userName)

    return True

def _saveHistory(rdb, sequenceName, used_id, userName):
    """"Save log data with user, requested id as well"""
    rdb.rpush('history', json.dumps({
        "sequence_name": sequenceName, 
        "sequence_value": used_id, 
        "user_name": userName, 
        "created_on":  str(datetime.now())
        }))
    return True