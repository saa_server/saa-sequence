from gevent import monkey; monkey.patch_all()

from bottle import run, get, default_app, debug
import bottle_redis

from controllers import sequence
import argparse


parser = argparse.ArgumentParser()

parser.add_argument('--host', default='0.0.0.0')
parser.add_argument('--port', default=15300)

parser.add_argument('--redis-host', dest='redishost', default='localhost')
parser.add_argument('--redis-port', dest='redisport', default=6379)

parser.add_argument('-d', '--debug', action='store_true')
args = parser.parse_args()

app = application = default_app()
redis_plugin = bottle_redis.RedisPlugin(host=args.redishost, port=args.redisport)
app.install(redis_plugin)

@get('/')
def index():
    return "Use route: /sequence/getNext/[sequenceName]?userName=...&secret=..."


DEBUG = args.debug

debug(DEBUG)

run(app, host=args.host, port=args.port, server="gevent", debug=DEBUG, reloader=DEBUG)