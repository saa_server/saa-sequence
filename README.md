# Simple Sequence Service

SSS provides unique sequentional number for custom essence.

### setup

* install redis
* populate redis with initial data

    ```
        ./init.sh
    ```

* install all requirements from [requirements.txt](requirements.txt)
    
    ```
        pip install -r requiremenets.txt
    ```

* run server
    ```
        python main.py
    ```


#### Usage

Check usage with 

```
    python main.py --help
```

#### Usage with circus

You can start app with [circus process manager](https://circus.readthedocs.io):

```
sudo apt-get install libzmq-dev libevent-dev python-dev python-virtualenv

virtualenv /tmp/circus
cd /tmp/circus
bin/pip install circus
bin/pip install circus-web
bin/pip install chaussette
```

start daemon

```
circusd --daemon circus.ini
```